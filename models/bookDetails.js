var mongoose = require('mongoose');
var Schema       = mongoose.Schema;
 
 var bookDetails = new Schema({
    book_name: { type : String ,required: true  },
    available: { type: Boolean ,default:true }
 });

 bookDetails.methods.toJSON =function(){
  var obj = this.toObject();
  delete obj.__v;
  delete obj._id;
  return obj;
   
}

module.exports=mongoose.model('bookDetails', bookDetails);
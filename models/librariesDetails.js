var mongoose = require('mongoose');
var Schema       = mongoose.Schema;
 
 var librariesDetails = new Schema({
    library_name: { type : String ,required: true  },
    books: [{ type: Schema.Types.ObjectId, ref: 'bookDetails' }],
    users: [{ type: Schema.Types.ObjectId, ref: 'userDetails'}]
 });

 librariesDetails.methods.toJSON =function(){
  var obj = this.toObject();
  delete obj.__v;
  delete obj._id;
  return obj;
   
}

module.exports=mongoose.model('librariesDetails', librariesDetails);
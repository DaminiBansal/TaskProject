var express = require('express'),
    router = express.Router();
app = express(),
    bodyParser = require('body-parser'),
    mongoose = require('mongoose');
var mongo = require('mongodb');
var MongoClient = mongo.MongoClient;
mongoose.Promise = require('bluebird');

var userDetails = require('./../models/userDetails');
var bookDetails = require('./../models/bookDetails');
var libraryDetails = require('./../models/librariesDetails');



app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
var ObjectId = require('mongodb').ObjectID;


var url = "mongodb://damini:123456@ds155582.mlab.com:55582/librarydb";
mongoose.connect(url, function(err, db) {

    if (err) {
        console.log(err);
    }

    router.post('/add-library', function(req, res, next) {
        var libraryDetailss = new libraryDetails();
        libraryDetailss.library_name = req.query.name;
        libraryDetailss.save(function(er) {
            if (er) {
                return next(er);
            } else {
                res.send("saved successfully");
            }
        });
    });

    router.post('/add-book', function(req, res, next) {
        var bookDetailss = new bookDetails();
        bookDetailss.book_name = req.query.bookname;
        bookDetailss.save(function(er) {
            if (er) {
                return next(er);
            } else {

                //var libraryDetailss = new libraryDetails();
                libraryDetails.findOneAndUpdate({ library_name: req.query.libraryname }, { $push: { "books": bookDetailss._id } }).exec(
                    function(err, doc) { /* <callback> */
                        if (err) res.json(err);
                        else {
                            res.send("saved suuccessfully");


                        }
                    }

                );
                //   res.send("saved successfully");

            }
        });
    });

    router.post('/add-user', function(req, res, next) {
        var userDetailss = new userDetails();
        userDetailss.user_name = req.query.username;
        userDetailss.save(function(er) {
            if (er) {
                return next(er);
            } else {

                //var libraryDetailss = new libraryDetails();
                libraryDetails.findOneAndUpdate({ library_name: req.query.libraryname }, { $push: { "users": userDetailss._id } }).exec(
                    function(err, doc) { /* <callback> */
                        if (err) res.json(err);
                        else {
                            res.send("saved suuccessfully");


                        }
                    }

                );
                res.send("saved successfully");

            }
        });
    });

    router.put('/issue-book', function(req, res, next) {
        //var bookDetailss = new bookDetails();
        bookDetails.findOneAndUpdate({ _id: ObjectId(req.headers.book_id) }, { available: false }).exec(
            function(err, doc) { /* <callback> */
                if (err) res.json(err);
                else {


                    //var userDetailss = new userDetails();
                    userDetails.findOneAndUpdate({ user_name: req.query.username }, { $push: { "books": ObjectId(req.headers.book_id) } }).exec(
                        function(err, doc) { /* <callback> */
                            if (err) res.json(err);
                            else {
                                res.send("saved suuccessfully");


                            }
                        }

                    );
                    //   res.send("saved successfully");

                }
            });
    });
    router.put('/return-book', function(req, res, next) {
        var bookDetailss = new bookDetails();
        bookDetails.findOneAndUpdate({ _id: ObjectId(req.headers.book_id) }, { available: true }).exec(
            function(err, doc) { /* <callback> */
                if (err) res.json(err);
                else {


                    var userDetailss = new userDetails();
                    userDetails.findOneAndUpdate({ user_name: req.query.username }, { $pull: { "books": ObjectId(req.headers.book_id) } }).exec(
                        function(err, doc) { /* <callback> */
                            if (err) res.json(err);
                            else {
                                res.send("saved suuccessfully");


                            }
                        }

                    );
                    // res.send("saved successfully");

                }
            });
    });

    router.get('/library-book-list', function(req, res, next) {
        var booklist = "";
        var ctr = 0;
        // var libraryDetailss = new libraryDetails();
        libraryDetails.findOne({ library_name: req.query.libraryname }).exec(
            function(err, doc) { /* <callback> */
                if (err) res.json(err);
                else {
                    if (doc.books.length > 0) {
                        doc.books.forEach(function(myDoc) {
                            bookDetails.findOne({ _id: myDoc, available: true }).exec(
                                function(err, docc) { /* <callback> */
                                    if (err) res.json(err);
                                    else {
                                        ctr++;
                                        booklist = booklist + docc.book_name + ",";
                                        // console.log(booklist);
                                        if (ctr == doc.books.length) {
                                            res.send("books list:" + booklist);
                                        }
                                    }
                                }

                            );


                        });
                    } else
                        res.send("books list:no book available");
                }
            });
    });



    router.get('/user-book-list', function(req, res, next) {
        var booklist = "";
        var ctr = 0;
        userDetails.findOne({ user_name: req.query.username }).exec(
            function(err, doc) { /* <callback> */
                if (err) res.json(err);
                else {

                    if (doc.books.length > 0) {
                        doc.books.forEach(function(myDoc) {
                            bookDetails.findOne({ _id: myDoc, available: true }).exec(
                                function(err, docc) { /* <callback> */
                                    if (err) res.json(err);
                                    else {
                                        ctr++;
                                        booklist = booklist + docc.book_name + ",";
                                        if (ctr == doc.books.length) {
                                            res.send("books list:" + booklist);
                                        }

                                    }
                                }

                            );


                        });
                    } else
                        res.send("books list:no book available");
                }
            });
    });
});
module.exports = router;